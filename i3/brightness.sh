#!/usr/bin/env bash

# Use xrandr to get brightness of main connected screen and increase
# or decrease it.
# N.B. this probably won't work properly with multiple screens

DEVICE=$(xrandr | grep " connected " | cut -d" " -f1)
BRIGHTNESS=$(xrandr --verbose | grep Brightness | cut -d" " -f2)
if [ "$1" == "+" ]; then
    NEW_BRIGHTNESS=$(echo "if (${BRIGHTNESS} < 0.9) ${BRIGHTNESS} + 0.1 else 1.0" | bc)
elif [ "$1" == "-" ]; then
    NEW_BRIGHTNESS=$(echo "if (${BRIGHTNESS} > 0.1) ${BRIGHTNESS} - 0.1 else 0.0" | bc)
fi

xrandr --output ${DEVICE} --brightness ${NEW_BRIGHTNESS}
