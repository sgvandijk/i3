#!/usr/bin/env bash

SINK=$(pactl info | grep "Default Sink" | cut -d" " -f3)
echo $SINK

if [ "$1" == "+" ]; then
    pactl set-sink-volume $SINK +5%
elif [ "$1" == "-" ]; then
    pactl set-sink-volume $SINK -5%
elif [ "$1" == "0" ]; then
    pactl set-sink-mute $SINK toggle
fi
