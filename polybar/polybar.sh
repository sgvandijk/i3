#!/usr/bin/env sh

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -x polybar >/dev/null; do sleep 1; done

# Check which monitor to run on
# The first reported by xrandr seems usually good
export MONITOR=$(xrandr | grep " connected " | cut -d" " -f1 | head -n 1)
echo $MONITOR

# Launch polybar
polybar sanderbar &

