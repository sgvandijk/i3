# i3

i3 window manager configuration

## Usage

    sudo apt install i3 \
        libcairo2-dev \
        libjsoncpp-dev \
        libpulse-dev \
        libxcb-composite0-dev \
        libxcb-cursor-dev \
        libxcb-ewmh-dev \
        libxcb-icccm4-dev \
        libxcb-util-dev \
        libxcb-xkb-dev \
        libxcb-xrm-dev \
        python-xcbgen \
        ttf-unifont \
        wireless-tools \
        xcb \
        xcb-proto

    # Clone, build and install polybar from https://github.com/polybar/polybar

    cd ~/.config
    ln -s path/to/i3/i3 .
    ln -s path/to/i3/polybar .

    # For hDPI, edit ~/.Xresources:
    Xft.dpi: 180

    # Edit /usr/share/X11/xorg.conf.d/40-libinput.conf and add to touchpad section:
        ...
        Option "Tapping" "on"
        Option "ClickMethod" "clickfinger"
        Option "NaturalScrolling" "true"
        ...
